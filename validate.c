
/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "ezxml.h"
#include "cgic.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define ALARM_COUNT		"zigorAlarmsPresent.0"
#define ALARM_ID		"zigorAlarmId."
#define ALARM_DESCR		"zigorAlarmDescr."
#define ALARM_TIME		"zigorAlarmTime."
#define ALARM_ELEM		"zigorAlarmElementList."
#define ALARM_COND		"zigorAlarmCondition."
#define SYS_LOCATION		"zigorSysLocation.0"
#define SYS_DATE			"zigorSysDate.0"
#define GEN_OIDS		ALARM_COUNT " " SYS_LOCATION " " SYS_DATE

#define GEN_XML_TEMPLATE	\
"<r> \n\
	\t <loc> %s </loc>\n\
	\t <na> %s </na>\n\
	\t <date> %s </date>\n\
</r> \n"

#define DASHBOARD_JSON_START	"{\n"
#define DASHBOARD_JSON_END	"\t\n}\n"
#define DASHBOARD_JSON_TEMPLATE	"\t\"%s\": %s "

#define MEASUREMENTS_JSON_START	"{\"Parameters\": [\n"
#define MEASUREMENTS_JSON_END	"\t]\n}\n"
#define MEASUREMENTS_JSON_TEMPLATE	\
	"\t{\n\
\t\t\"description\": \"%s\",\n\
\t\t\"value\": \"%s\"\n\
\t}"

#define SETTINGS_JSON_START	"{\"Parameters\": [\n"
#define SETTINGS_JSON_END	"\t]\n}\n"
#define SETTINGS_JSON_TEMPLATE	\
	"\t{\n\
\t\t\"name\": \"%s\",\n\
\t\t\"oid\": \"%s\",\n\
\t\t\"values\": %s,\n\
\t\t\"units\": \"%s\",\n\
\t\t\"factor\": \"%s\",\n\
\t\t\"minmax\": \"%s\",\n\
\t\t\"edit\": %s,\n\
\t\t\"type\": \"%s\",\n\
\t\t\"value\": \"%s\"\n\
\t}"

#define ALARMS_JSON_START	"{\"Active\": [\n"
#define ALARMS_JSON_END	"\t]\n}\n"
#define ALARMS_JSON_TEMPLATE	\
	"\t{\n\
\t\t\"ID\": \"%s\",\n\
\t\t\"Descr\": \"%s\",\n\
\t\t\"Time\": \"%s\",\n\
\t\t\"Elem\": %s,\n\
\t\t\"Cond\": \"%s\"\n\
\t}"

#define CMD_SNMPGET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPGET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPGET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPGET 				"snmpget -OqvU -v 2c -c zadmin localhost %s"

#define CMD_SNMPGET_VARS			CMD_SNMPGET_VAR_MIBDIR " " CMD_SNMPGET_VAR_MIBS " " CMD_SNMPGET_VAR_LD_LIB
#define GET_CMD							CMD_SNMPGET_VARS " " CMD_SNMPGET

#define CMD_SNMPSET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPSET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPSET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPSET 				"snmpset -OqvU -v 2c -c zadmin localhost %s %s %s"

#define CMD_SNMPSET_VARS			CMD_SNMPSET_VAR_MIBDIR " " CMD_SNMPSET_VAR_MIBS " " CMD_SNMPSET_VAR_LD_LIB
#define SET_CMD							CMD_SNMPSET_VARS " " CMD_SNMPSET

#define MAX_SNMPGET_RESPONSE_LEN	1024

/* Global Variables, used through the program to store incoming form elements */
char pass[16];
char user[16];
char lang[16];
char menu[32];
char site[64];
char xml[16];
/* Global Variables, used through the program to store incoming form elements */

/* Random key generator */
static char * rand_string(char *str, size_t size){
	const char charset[] = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM!@\"?*{}()$%&/1234567890";
	if (size){
		--size;
		for (size_t n = 0; n < size; n++){
			int key = rand() % (int) (sizeof charset - 1);
			str[n] = charset[key];
		}
		str[size]='\0';
	}
	return str;
}

char * set_oid_value(char const * const oid, char const * const value, char const * const type)
{
	FILE * process;
	char * response = NULL;
	char * snmpset_cmd = NULL;
	int i;
	char const *type_str;

	if (strcmp(type, "str") == 0)
		type_str = "s";
	else if (strcmp(type, "int") == 0)
		type_str = "i";
	else
		type_str = "i"; /* Try with integer... */

	asprintf(&snmpset_cmd, SET_CMD, oid, type_str, value);
	if (snmpset_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	printf("<p>%s</p>\n", snmpset_cmd);
	process = popen(snmpset_cmd, "r");
	free(snmpset_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] != '\n') {
			response[i] = data[0];
		} else {
			break;
		}
	}
	response[i] = '\0';

	pclose(process);
	return response;
}

void clear_log(){
	FILE * fp;
	fp = fopen("../../ags-sepec/share/config/changelog-sepec.lua", "w");
	fprintf(fp, "local changelog = {}\n\n");
	//fclose(fp);
	//fp = fopen("../../ags-sepec/share/config/changelog-sepec.lua", "a");
	fprintf(fp, "return changelog,0,0\n");
	fclose(fp);
	popen("mv /home/user/changelog.csv /home/user/changelog-`date +\"%d-%m-%Y-%H%M%S\"`.csv", "r");
	popen("zip /home/user/changelog.zip /home/user/changelog-*.csv", "r");
	popen("rm /home/user/changelog*.csv", "r");
	popen("echo  > /home/user/changelog.csv", "r");
	fprintf(cgiOut, "CLEARED");
	set_oid_value("zigorCtrlParamState.0", "1", "int");
	exit(EXIT_SUCCESS);
}

char * get_oid_value(char const * const oid)
{
	FILE * process;
	char * response = NULL;
	char * snmpget_cmd = NULL;
	int i;

	asprintf(&snmpget_cmd, GET_CMD, oid);
	if (snmpget_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	process = popen(snmpget_cmd, "r");
	free(snmpget_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (data[0] == '\n' && response[i-1] == '\n'){
			break;
		}
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] == '\n') {
			response[i] = data[0];
		} 
		if (data[0] != '\n') {
			response[i] = data[0];
		}
	}
	response[i] = '\0';
	pclose(process);
	return response;
}

void translate_descr(char * description, char * ID){
    int i, j;
	ezxml_t root;
	ezxml_t node;
	ezxml_t param;
	char lang[1024];
	
	root = ezxml_parse_file("alarms_description.xml");
	cgiFormString("lang", lang, sizeof lang);
	
    for(i = 0; description[i] != '\0'; ++i)
    {
        while (!( (description[i] >= '0' && description[i] <= '9') || (description[i] == '.') || description[i] == '\0') )
        {
            for(j = i; description[j] != '\0'; ++j)
            {
                description[j] = description[j+1];
            }
            description[j] = '\0';
        }
    }
    
    for (node = ezxml_child(root, "node"); node; node = node->next) {
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			if (strcmp(description, oid) == 0){
				strcpy(description, ezxml_attr(param, lang));
				strcpy(ID, ezxml_attr(param, "code"));
			}
		}
	}
}

void get_general() {
	char *oids = strtok (get_oid_value(GEN_OIDS), "\n");
	char *array[15];
	int i = 0;
	
	while (oids != NULL){
		array[i++] = oids;
		oids = strtok (NULL, "\n");
	}
	
	int acAlarms = atoi(array[0]);
	char *loc = array[1];
	char *na = NULL;
	char *date = array[2];
	
	cgiHeaderContentType("text/xml");

	asprintf(&na, "%d", acAlarms);

	fprintf(cgiOut, GEN_XML_TEMPLATE, loc, na, date);
	free(na);
	exit(EXIT_SUCCESS);
}

void get_dashboard() {
	cgiHeaderContentType("application/json");
	
	ezxml_t root;
	ezxml_t node;
	
	char filename[1024];
	char oidsToGet[1024];
	
	int i = 0;
	
	cgiFormResultType result = cgiFormString("dashboard_xml", filename, sizeof filename);

	if (result != cgiFormSuccess) {
		strcpy(filename, "dashboard.xml");
	}
	
	root = ezxml_parse_file(filename);
	for (node = ezxml_child(root, "node"); node; node = node->next){
		i = 0;
		ezxml_t param;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			strcat(oidsToGet, oid);
			strcat(oidsToGet, " ");
		}
	}
	
	char *oids = strtok (get_oid_value(oidsToGet), "\n");
	char *array[15];
	
	while (oids != NULL){
		array[i++] = oids;
 		oids = strtok (NULL, "\n");
	}
	
	fprintf(cgiOut, DASHBOARD_JSON_START);
	for (node = ezxml_child(root, "node"); node; node = node->next) {
		ezxml_t param;
		i = 0;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * description = ezxml_attr(param, "name");
			char const * units = ezxml_attr(param, "units");
			char const * factor = ezxml_attr(param, "factor");
			char *value = NULL;
			bool const is_last = param->next == NULL && node->next == NULL ? true : false;

			value = array[i];

			if (factor) {
				int factor_int = atoi(factor);
				float value_float = atoi(value);
				value_float = value_float / factor_int;
				char *new_value = NULL;
				asprintf(&new_value, "%.01f", value_float);
				value = new_value;
			}

			if (units) {
				char *new_value = NULL;
				asprintf(&new_value, "\"%s %s\"", value, units);
				value = new_value;
			}
			fprintf(cgiOut, DASHBOARD_JSON_TEMPLATE "%s\n", description, value, is_last ? "" : ",");
			i++;
		}
	}
	ezxml_free(root);
	fprintf(cgiOut, DASHBOARD_JSON_END);
	
	exit(EXIT_SUCCESS);
}

void get_measurements() {
	cgiHeaderContentType("application/json");
	
	ezxml_t root;
	ezxml_t node;
	
	char filename[1024];
	char username[1024];
	char oidsToGet[1024];
	char lang[1024];
	
	int i = 0;
	
	cgiFormResultType result = cgiFormString("measurements_xml", filename, sizeof filename);
	cgiFormString("username", username, sizeof username);
	cgiFormString("lang", lang, sizeof lang);
	
	if (result != cgiFormSuccess) {
		strcpy(filename, "measurements_general.xml");
	}
	
	root = ezxml_parse_file(filename);
	for (node = ezxml_child(root, "node"); node; node = node->next){
		i = 0;
		ezxml_t param;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			char const * auth = ezxml_attr(param, "auth");
			if(!ezxml_attr(param, "auth")){
				strcat(oidsToGet, oid);
				strcat(oidsToGet, " ");
			}else if (strcmp(username, auth) ==0 ) {
				strcat(oidsToGet, oid);
				strcat(oidsToGet, " ");
			}
		}
	}
	
	char *oids = strtok (get_oid_value(oidsToGet), "\n");
	char *array[15];
	
	while (oids != NULL){
		array[i++] = oids;
		oids = strtok (NULL, "\n");
	}
	
	fprintf(cgiOut, MEASUREMENTS_JSON_START);
	for (node = ezxml_child(root, "node"); node; node = node->next) {
		ezxml_t param;
		i = 0;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * auth = ezxml_attr(param, "auth");
			char const * units = ezxml_attr(param, "units");
			char const * factor = ezxml_attr(param, "factor");
			char *value = NULL;
			bool const is_last = param->next == NULL && node->next == NULL ? true : false;
			if(!ezxml_attr(param, "auth")){
				char const * description = ezxml_attr(param, lang);
				value = array[i];
				if (factor) {
					int factor_int = atoi(factor);
					float value_float = atoi(value);
					value_float = value_float / factor_int;
					char *new_value = NULL;
					asprintf(&new_value, "%.01f", value_float);
					value = new_value;
				}

				if (units) {
					char *new_value = NULL;
					asprintf(&new_value, "%s %s", value, units);
					value = new_value;
				}
				fprintf(cgiOut, MEASUREMENTS_JSON_TEMPLATE "%s\n", description, value, is_last ? "" : ",");
				i++;
			}else if (strcmp(username, auth) ==0 ) {
				char const * description = ezxml_attr(param, lang);
				value = array[i];
				if (factor) {
					int factor_int = atoi(factor);
					float value_float = atoi(value);
					value_float = value_float / factor_int;
					char *new_value = NULL;
					asprintf(&new_value, "%.01f", value_float);
					value = new_value;
				}

				if (units) {
					char *new_value = NULL;
					asprintf(&new_value, "%s %s", value, units);
					value = new_value;
				}
				fprintf(cgiOut, MEASUREMENTS_JSON_TEMPLATE "%s\n", description, value, is_last ? "" : ",");
				i++;
			}
		}
	}
	ezxml_free(root);
	fprintf(cgiOut, MEASUREMENTS_JSON_END);
	
	exit(EXIT_SUCCESS);
}

void get_settings() {
	cgiHeaderContentType("application/json");
	/* Top of the page */
	ezxml_t root;
	ezxml_t node;
	char filename[1024];
	char username[1024];
	char oidsToGet[4096];
	char lang[1024];
	int i = 0;
	
	cgiFormResultType result = cgiFormString("settings_xml", filename, sizeof filename);
	cgiFormString("username", username, sizeof username);
	cgiFormString("lang", lang, sizeof lang);

	if (result != cgiFormSuccess) {
		strcpy(filename, "settings_general.xml");
	}
	
	root = ezxml_parse_file(filename);
	for (node = ezxml_child(root, "node"); node; node = node->next){
		i = 0;
		ezxml_t param;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			char const * auth = ezxml_attr(param, "auth");
			if(!ezxml_attr(param, "auth")){
				strcat(oidsToGet, oid);
				strcat(oidsToGet, " ");
			}else if (strcmp(username, auth) == 0 ) {
				strcat(oidsToGet, oid);
				strcat(oidsToGet, " ");
			}
		}
	}

	char *oids = strtok (get_oid_value(oidsToGet), "\n");
	char *array[23];
	
	while (oids != NULL){
		array[i++] = oids;
		oids = strtok (NULL, "\n");
	}
	
	fprintf(cgiOut, SETTINGS_JSON_START);
	for (node = ezxml_child(root, "node"); node; node = node->next) {
		ezxml_t param;
		i = 0;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * auth = ezxml_attr(param, "auth");
			char *value = NULL;
			bool const is_last = param->next == NULL && node->next == NULL ? true : false;
			if(!ezxml_attr(param, "auth")){
				char const * name = ezxml_attr(param, lang);
				char const * values = ezxml_attr(param, "values");
				char const * units = ezxml_attr(param, "units");
				char const * oid = ezxml_attr(param, "oid");
				char const * factor = ezxml_attr(param, "factor");
				char const * minmax = ezxml_attr(param, "minmax");
				char const * edit = ezxml_attr(param, "edit");
				char const * type = ezxml_attr(param, "type");
				value = array[i];
				if (values[0] == '\0')
					values = "\"\"";
				if (value == NULL)
					value = "";
				if (strcmp(type, "int") == 0 && factor[0] != '\0') {
					int factor_int = atoi(factor);
					float value_float = atoi(value);
					value_float = value_float / factor_int;
					char *new_value = NULL;

					asprintf(&new_value, "%.01f", value_float);
					value = new_value;
				}
				fprintf(cgiOut, SETTINGS_JSON_TEMPLATE "%s\n", name, oid, values, units, factor, minmax, edit, type, value, is_last ? "" : ",");
				i++;
			}else if (strcmp(username, auth) ==0 ) {
				char const * name = ezxml_attr(param, lang);
				char const * values = ezxml_attr(param, "values");
				char const * units = ezxml_attr(param, "units");
				char const * oid = ezxml_attr(param, "oid");
				char const * factor = ezxml_attr(param, "factor");
				char const * minmax = ezxml_attr(param, "minmax");
				char const * edit = ezxml_attr(param, "edit");
				char const * type = ezxml_attr(param, "type");
				value = array[i];
				if (values[0] == '\0')
					values = "\"\"";
				if (value == NULL)
					value = "";
				if (strcmp(type, "int") == 0 && factor[0] != '\0') {
					int factor_int = atoi(factor);
					float value_float = atoi(value);
					value_float = value_float / factor_int;
					char *new_value = NULL;

					asprintf(&new_value, "%.01f", value_float);
					value = new_value;
				}
				fprintf(cgiOut, SETTINGS_JSON_TEMPLATE "%s\n", name, oid, values, units, factor, minmax, edit, type, value, is_last ? "" : ",");
				i++;
			}
		}
	}
	ezxml_free(root);
	fprintf(cgiOut, SETTINGS_JSON_END);

	exit(EXIT_SUCCESS);
}

void get_alarms() {
	cgiHeaderContentType("application/json");
	/* Top of the page */
	char oidsToGet[1024];
	char *array[64];
	int i = 0;
	char *n = NULL;
	
	int alarmCount = atoi(get_oid_value(ALARM_COUNT));
	
	fprintf(cgiOut, ALARMS_JSON_START);
	for (i = 1; i <= alarmCount; i++){
		asprintf(&n, "%d", i);
		strcat(oidsToGet, ALARM_ID);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_DESCR);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_TIME);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_ELEM);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_COND);
		strcat(oidsToGet, n);
		char *oids = strtok (get_oid_value(oidsToGet), "\n");
		
		int a = 0;
		while (oids != NULL){
			array[a++] = oids;
			oids = strtok (NULL, "\n");
		}
		
		char * ELEM = array[3];
		char * time = array[2];
		char * cond = array[4];
		char * descr = array[1];
		char * id = NULL;
		id = malloc(2);
		translate_descr(descr, id);
		bool const is_last = i == alarmCount ? true : false;
		fprintf(cgiOut, ALARMS_JSON_TEMPLATE "%s\n", id, descr, time, ELEM, cond, is_last ? "" : ",");
		memset(oidsToGet, 0, sizeof oidsToGet);
		memset(array, 0, 64 * (sizeof array[0]) );
		free(n);
	}
	fprintf(cgiOut, ALARMS_JSON_END);
	exit(EXIT_SUCCESS);
}

void set_param() {
	char oid[1024];
	char value[1024];
	char type[1024];
	char access[1024];

	cgiFormResultType result;

	cgiHeaderContentType("text/html");
	fprintf(cgiOut, "<html><head>\n");
	fprintf(cgiOut, "<title>DSP Set configuration</title></head>\n");
	fprintf(cgiOut, "<body><h1>DSP Set configuration</h1>\n");
	result = cgiFormString("access", access, sizeof access);
	fprintf(cgiOut, "<p>Access [%d]: %s</p>\n", result, access);
	result = cgiFormString("oid", oid, sizeof oid);
	fprintf(cgiOut, "<p>OID [%d]: %s</p>\n", result, oid);
	result = cgiFormString("value", value, sizeof value);
	fprintf(cgiOut, "<p>value [%d]: %s</p>\n", result, value);
	result = cgiFormString("type", type, sizeof type);
	fprintf(cgiOut, "<p>type [%d]: %s</p>\n", result, type);
	set_oid_value("zigorParamChangesLevelAccess.0", access, type);
	set_oid_value("zigorParamChangesInterface.0", "1", type);
	set_oid_value(oid, value, type);
	fprintf(cgiOut, "</body></html>\n");

	exit(EXIT_SUCCESS);
}

/* Function to handle user requests for sites. Checks if the cookies are valid */
void handleUsers(char * username_cookie, char * profile_cookie){
	FILE * fp;
	time_t t = time(NULL);
	char * line = NULL;
	t = time(0);
	size_t len = 0;
    ssize_t read;
	
	fp = fopen("validusers.txt", "r");
		
		while ((read = getline(&line, &len, fp)) != -1){
			
			char *word = strtok (line, ";");
			char *array[8];
			int i = 0;
	
			while (word != NULL){
				array[i++] = word;
				word = strtok (NULL, ";");
			}
			
			
			if(strcmp(array[0], profile_cookie) == 0){
				if(strcmp(array[1], username_cookie) == 0){
					time_t time_int = atoi(array[2]);
					if ( t < time_int ){
						return;
					}else {
						cgiHeaderCookieSetString("username", "", 0, "/", SERVER_NAME);
						cgiHeaderCookieSetString("profile", "", 0, "/", SERVER_NAME);
					}
				}
			}
		}
		
		fclose(fp);
		if (line)
			free(line);
		
		cgiHeaderCookieSetString("username", "", 0, "/", SERVER_NAME);
		cgiHeaderCookieSetString("profile", "", 0, "/", SERVER_NAME);
}

/* Handle the submission of authentication form data
 * Compares the incoming form to the struct data and
 * sets the language for the whole odmian using Cookies */
void handleSubmit(){
	FILE * fp;
	ezxml_t root;
	ezxml_t node;
	time_t t;
	t = time(0);
	char line[1024];
	char random_id[32];
	char * time = NULL;
	char username_cookie[32];
	char profile_cookie[32];
	
	if ( cgiCookieString( "username", username_cookie, 32) != cgiFormNotFound 
		&& cgiCookieString( "profile", profile_cookie, 32) != cgiFormNotFound 
		&& cgiFormString("lang", lang, sizeof pass) != cgiFormNotFound){
		
		handleUsers(username_cookie, profile_cookie);
		return;
		
	}
	
	cgiHeaderCookieSetString("username", "", 0, "/", SERVER_NAME);
	cgiHeaderCookieSetString("profile", "", 0, "/", SERVER_NAME);
	cgiHeaderCookieSetString("language", "", 0, "/", SERVER_NAME);
		
	cgiFormString("pass", pass, sizeof pass);
	cgiFormString("user", user, sizeof user);
	if (cgiFormString("lang", lang, sizeof pass) != cgiFormNotFound){
		cgiFormString("lang", lang, sizeof lang);
	} else {
		cgiCookieString( "language", lang, 32);
	}
	root = ezxml_parse_file("validusers.xml");
	for (node = ezxml_child(root, user); node; node = node->next){
		char const * password = ezxml_attr(node, "pass");
		char const * Access = ezxml_attr(node, "access");
		char access[32];
		strcpy(access, Access);
		fp = fopen("validusers.txt", "a");
		
		if (strcmp(pass, password) == 0){
			
			rand_string(random_id, 9);

			asprintf(&time, "%ld", t + 86400);
			
			cgiHeaderCookieSetString("username", random_id, 86400, "/", SERVER_NAME); //testing this sh*t
			cgiHeaderCookieSetString("profile", user, 86400, "/", SERVER_NAME);
			cgiHeaderCookieSetString("language", lang, 86400, "/", SERVER_NAME);
			cgiHeaderCookieSetString("access", access, 86400, "/", SERVER_NAME);
			
			
			strcat(line, user);
			strcat(line, ";");
			strcat(line, random_id);
			strcat(line, ";");
			strcat(line, time);
			strcat(line, "\n");
			
			fprintf(fp, "%s", line);
			
			break;
			
		}else { 
			
			cgiHeaderCookieSetString("username", "", 0, "/", SERVER_NAME);
			cgiHeaderCookieSetString("profile", "", 0, "/", SERVER_NAME);
			
		}
	}
	fclose(fp);
	ezxml_free(root);
}

/* Function to generate the site html by reading from a file
 * then writing it a empty file. This uses an IF statement 
 * to generate the different htmls from the selection on the menu */
void readFile(){
    FILE * fp;
	ezxml_t root;
	ezxml_t node;
	ezxml_t param;
	char lang[16];
    char * line = NULL;
	char const * word = NULL;
	char buffer[32];
    size_t len = 0;
    ssize_t read;
	
	cgiHeaderContentType("text/html");
	
	if (cgiFormString("site", site, sizeof site) != cgiFormNotFound){
		if (cgiFormString("lang", lang, sizeof pass) != cgiFormNotFound){
			cgiFormString("lang", lang, sizeof lang);
		} else {
			cgiCookieString( "language", lang, 32);
		}
		cgiFormString("menu", menu, sizeof menu);
		root = ezxml_parse_file("dictionary.xml");
		fp = fopen(site, "r");
		if (fp == NULL){
			fprintf(cgiOut, "FAIL, no site detected, current site is %s\n", site);
			exit(EXIT_FAILURE);
		}
			while ((read = getline(&line, &len, fp)) != -1) {
				if(strstr(line, "$$") != 0){
					for (node = ezxml_child(root, menu); node; node = node->next){
						for (param = ezxml_child(node, "var"); param; param = param->next) {
							char const * compare_str = ezxml_attr(param, "str");
							strcpy(buffer, compare_str);
							if(strstr(line, buffer)){
								word = ezxml_attr(param, lang);
								strcpy(line, word);
								break;
							}
						}
					}
				}
			fprintf(cgiOut, "%s", line);
			}
		
	}else{
		if (cgiFormString("lang", lang, sizeof pass) != cgiFormNotFound){
			cgiFormString("lang", lang, sizeof lang);
		} else {
			cgiCookieString( "language", lang, 32);
		}
		root = ezxml_parse_file("dictionary.xml");
		fp = fopen("../html/Redesign/dashboard.html", "r");
		if (fp == NULL){
			fprintf(cgiOut, "FAIL\n");
			exit(EXIT_FAILURE);
		}
		while ((read = getline(&line, &len, fp)) != -1) {
			if(strstr(line, "$$") != 0){
				for (node = ezxml_child(root, "Dashboard"); node; node = node->next){
					for (param = ezxml_child(node, "var"); param; param = param->next) {
						char const * compare_str = ezxml_attr(param, "str");
						strcpy(buffer, compare_str);
						if(strstr(line, buffer)){
							word = ezxml_attr(param, lang);
							strcpy(line, word);
							break;
						}
					}
				}
			}
		fprintf(cgiOut, "%s", line);
		}
	}
    
	ezxml_free(root);
    fclose(fp);
    if (line)
        free(line);
	
	exit(EXIT_SUCCESS);
}


/* Main CGI function */
int cgiMain() {
	
	srand(time(NULL));
	
	char username_cookie[32];
	char profile_cookie[32];
	
	if (cgiFormString("general", xml, sizeof xml) != cgiFormNotFound){
		
		get_general();
		
	}else if (cgiFormString("dashboard_xml", xml, sizeof xml) != cgiFormNotFound){
		
		get_dashboard();
		
	}else if (cgiFormString("measurements_xml", xml, sizeof xml) != cgiFormNotFound){
		
		get_measurements();
		
	}else if (cgiFormString("settings_xml", xml, sizeof xml) != cgiFormNotFound){
		
		get_settings();
		
	}else if ( cgiFormString( "oid", xml, sizeof xml) != cgiFormNotFound ) {
		
		set_param();
		
	}else if ( cgiFormString( "clear_log", xml, sizeof xml) != cgiFormNotFound ) {
		
		clear_log();
		
	}else if (cgiFormString("alarms_active", xml, sizeof xml) != cgiFormNotFound){
		
		get_alarms();
		
	}else if (cgiFormString("pass", pass, sizeof pass) != cgiFormNotFound 
		&& cgiFormString("user", user, sizeof pass) != cgiFormNotFound){

		handleSubmit();
		
	}else if ( cgiCookieString( "username", username_cookie, 32) != cgiFormNotFound 
		&& cgiCookieString( "profile", profile_cookie, 32) != cgiFormNotFound ){
		
		handleUsers(username_cookie, profile_cookie);
		
	}else{
		
		return(EXIT_FAILURE);
		
	}
	
	setbuf(stdout, NULL);
	
	readFile();
	
	
	return 0;
}
