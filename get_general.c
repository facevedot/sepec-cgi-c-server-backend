
/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ezxml.h"
#include "cgic.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define ACTIVE_ALARMS		"zigorAlarmsPresent.0"
#define SYS_LOCATION		"zigorSysLocation.0"
#define SYS_DATE			"zigorSysDate.0"
#define ALL_OIDS		ACTIVE_ALARMS " " SYS_LOCATION " " SYS_DATE


#define XML_TEMPLATE	\
"<r> \n\
	\t <loc> %s </loc>\n\
	\t <na> %s </na>\n\
	\t <date> %s </date>\n\
</r> \n"

#define CMD_SNMPGET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPGET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPGET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPGET 				"snmpget -OqvU -v 2c -c zadmin localhost %s"

#define CMD_SNMPGET_VARS			CMD_SNMPGET_VAR_MIBDIR " " CMD_SNMPGET_VAR_MIBS " " CMD_SNMPGET_VAR_LD_LIB
#define CMD							CMD_SNMPGET_VARS " " CMD_SNMPGET

#define MAX_SNMPGET_RESPONSE_LEN	1024

char * get_oid_value(char const * const oid)
{
	FILE * process;
	char * response = NULL;
	char * snmpget_cmd = NULL;
	int i;

	asprintf(&snmpget_cmd, CMD, oid);
	if (snmpget_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	process = popen(snmpget_cmd, "r");
	free(snmpget_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (data[0] == '\n' && response[i-1] == '\n'){
			break;
		}
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] == '\n') {
			response[i] = data[0];
		} 
		if (data[0] != '\n') {
			response[i] = data[0];
		}
	}
	response[i] = '\0';
	pclose(process);
	return response;
}

int cgiMain() {
	char *oids = strtok (get_oid_value(ALL_OIDS), "\n");
	char *array[15];
	int i = 0;
	
	while (oids != NULL){
		array[i++] = oids;
		oids = strtok (NULL, "\n");
	}
	
	int acAlarms = atoi(array[0]);
	char *loc = array[1];
	char *na = NULL;
	char *date = array[2];
	
	cgiHeaderContentType("text/xml");

	asprintf(&na, "%d", acAlarms);

	fprintf(cgiOut, XML_TEMPLATE, loc, na, date);
	free(na);
	return EXIT_SUCCESS;
}
