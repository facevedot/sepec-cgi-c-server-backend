CFLAGS=-g -Wall
CC=gcc
AR=ar
RANLIB=ranlib
LIBS=-L./ -lcgic -lezxml

all: libcgic.a cgictest.cgi get_params.cgi set_dsp_config.cgi capture upload_dsp_ini.cgi update_dsp_fw.cgi set_params.cgi dashboard.cgi get_measurements.cgi

install: libcgic.a
	cp libcgic.a /usr/local/lib
	cp cgic.h /usr/local/include
	@echo libcgic.a is in /usr/local/lib. cgic.h is in /usr/local/include.

libcgic.a: cgic.o cgic.h
	rm -f libcgic.a
	$(AR) rc libcgic.a cgic.o
	$(RANLIB) libcgic.a

#mingw32 and cygwin users: replace .cgi with .exe

cgictest.cgi: cgictest.o libcgic.a
	gcc cgictest.o -o cgictest.cgi ${LIBS}

get_params.cgi: get_params.o libcgic.a
	gcc get_params.o -o get_params.cgi ${LIBS}

get_measurements.cgi: get_measurements.o libcgic.a
	gcc get_measurements.o -o get_measurements.cgi ${LIBS}

get_general.cgi: get_general.o libcgic.a
	gcc get_general.o -o get_general.cgi ${LIBS}
	
get_alarms.cgi: get_alarms.o libcgic.a
	gcc get_alarms.o -o get_alarms.cgi ${LIBS}


dashboard.cgi: dashboard.o libcgic.a
	gcc dashboard.o -o dashboard.cgi ${LIBS}
	
get_dashboard.cgi: get_dashboard.o libcgic.a
	gcc get_dashboard.o -o get_dashboard.cgi ${LIBS}
	
set_params.cgi: set_params.o libcgic.a
	gcc set_params.o -o set_params.cgi ${LIBS}

validate.cgi: validate.o libcgic.a
	gcc validate.o -o validate.cgi ${LIBS}

set_dsp_config.cgi: set_dsp_config.o libcgic.a
	gcc set_dsp_config.o -o set_dsp_config.cgi ${LIBS}

update_dsp_fw.cgi: update_dsp_fw.o libcgic.a
	gcc update_dsp_fw.o -o update_dsp_fw.cgi ${LIBS}

upload_dsp_ini.cgi: upload_dsp_ini.o libcgic.a
	gcc upload_dsp_ini.o -o upload_dsp_ini.cgi ${LIBS}

capture: capture.o libcgic.a
	gcc capture.o -o capture ${LIBS}

clean:
	rm -f *.o libcgic.a *.cgi capture cgicunittest

test:
	gcc -D UNIT_TEST=1 cgic.c -o cgicunittest
	./cgicunittest
