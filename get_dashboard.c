
/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ezxml.h"
#include "cgic.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define JSON_START	"{\n"
#define JSON_END	"\t\n}\n"
#define JSON_TEMPLATE	"\t\"%s\": %s "


#define CMD_SNMPGET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPGET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPGET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPGET 				"snmpget -OqvU -v 2c -c zadmin localhost %s"

#define CMD_SNMPGET_VARS			CMD_SNMPGET_VAR_MIBDIR " " CMD_SNMPGET_VAR_MIBS " " CMD_SNMPGET_VAR_LD_LIB
#define CMD							CMD_SNMPGET_VARS " " CMD_SNMPGET

#define MAX_SNMPGET_RESPONSE_LEN	1024

char * get_oid_value(char const * const oid)
{
	FILE * process;
	char * response = NULL;
	char * snmpget_cmd = NULL;
	int i;

	asprintf(&snmpget_cmd, CMD, oid);
	if (snmpget_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	process = popen(snmpget_cmd, "r");
	free(snmpget_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (data[0] == '\n' && response[i-1] == '\n'){
			break;
		}
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] == '\n') {
			response[i] = data[0];
		} 
		if (data[0] != '\n') {
			response[i] = data[0];
		}
	}
	response[i] = '\0';
	pclose(process);
	return response;
}

int cgiMain() {
	cgiHeaderContentType("application/json");
	
	ezxml_t root;
	ezxml_t node;
	
	char filename[1024];
	char oidsToGet[1024];
	
	int i = 0;
	
	cgiFormResultType result = cgiFormString("measurements_xml", filename, sizeof filename);

	if (result != cgiFormSuccess) {
		strcpy(filename, "dashboard.xml");
	}
	
	root = ezxml_parse_file(filename);
	for (node = ezxml_child(root, "node"); node; node = node->next){
		i = 0;
		ezxml_t param;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			strcat(oidsToGet, oid);
			strcat(oidsToGet, " ");
		}
	}
	
	char *oids = strtok (get_oid_value(oidsToGet), "\n");
	char *array[15];
	
	while (oids != NULL){
		array[i++] = oids;
 		oids = strtok (NULL, "\n");
	}
	
	fprintf(cgiOut, JSON_START);
	for (node = ezxml_child(root, "node"); node; node = node->next) {
		ezxml_t param;
		i = 0;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * description = ezxml_attr(param, "name");
			char const * units = ezxml_attr(param, "units");
			char const * factor = ezxml_attr(param, "factor");
			char *value = NULL;
			bool const is_last = param->next == NULL && node->next == NULL ? true : false;

			value = array[i];

			if (factor) {
				int factor_int = atoi(factor);
				float value_float = atoi(value);
				value_float = value_float / factor_int;
				char *new_value = NULL;
				asprintf(&new_value, "%.01f", value_float);
				value = new_value;
			}

			if (units) {
				char *new_value = NULL;
				asprintf(&new_value, "\"%s %s\"", value, units);
				value = new_value;
			}
			fprintf(cgiOut, JSON_TEMPLATE "%s\n", description, value, is_last ? "" : ",");
			i++;
		}
	}
	ezxml_free(root);
	fprintf(cgiOut, JSON_END);
	
	return EXIT_SUCCESS;
}
