/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include "cgic.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define CMD_SNMPSET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPSET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPSET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPSET 				"snmpset -OqvU -v 2c -c zadmin localhost %s %s %s"

#define CMD_SNMPSET_VARS			CMD_SNMPSET_VAR_MIBDIR " " CMD_SNMPSET_VAR_MIBS " " CMD_SNMPSET_VAR_LD_LIB
#define CMD							CMD_SNMPSET_VARS " " CMD_SNMPSET

#define MAX_SNMPGET_RESPONSE_LEN	1024

char * set_oid_value(char const * const oid, char const * const value, char const * const type)
{
	FILE * process;
	char * response = NULL;
	char * snmpset_cmd = NULL;
	int i;
	char const *type_str;

	if (strcmp(type, "str") == 0)
		type_str = "s";
	else if (strcmp(type, "int") == 0)
		type_str = "i";
	else
		type_str = "i"; /* Try with integer... */

	asprintf(&snmpset_cmd, CMD, oid, type_str, value);
	if (snmpset_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	printf("<p>%s</p>\n", snmpset_cmd);
	process = popen(snmpset_cmd, "r");
	free(snmpset_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] != '\n') {
			response[i] = data[0];
		} else {
			break;
		}
	}
	response[i] = '\0';

	pclose(process);
	return response;
}

int cgiMain() {
	char oid[1024];
	char value[1024];
	char type[1024];

	cgiFormResultType result;

	cgiHeaderContentType("text/html");
	fprintf(cgiOut, "<html><head>\n");
	fprintf(cgiOut, "<title>DSP Set configuration</title></head>\n");
	fprintf(cgiOut, "<body><h1>DSP Set configuration</h1>\n");
	result = cgiFormString("oid", oid, sizeof oid);
	fprintf(cgiOut, "<p>OID [%d]: %s</p>\n", result, oid);
	result = cgiFormString("value", value, sizeof value);
	fprintf(cgiOut, "<p>value [%d]: %s</p>\n", result, value);
	result = cgiFormString("type", type, sizeof type);
	fprintf(cgiOut, "<p>type [%d]: %s</p>\n", result, type);
	set_oid_value(oid, value, type);
	fprintf(cgiOut, "</body></html>\n");

	return 0;
}
