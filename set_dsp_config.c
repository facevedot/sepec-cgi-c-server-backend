/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include "cgic.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define MAX_SNMPGET_RESPONSE_LEN	32

#define INI_FILE_PATH		"/tmp/dsp.ini"
#define PUBLIC_FILE_PATH	"/tmp/publica.txt"
#define PRIVATE_FILE_PATH	"/tmp/privada.txt"
#define SERIAL_PORT			"/dev/ttyS0"
#define DSP_FICH_BIN_PATH	"/usr/local/zigor/activa/tools/dsp_fich"
#define NEW_CFG_PATH		"/tmp/new_cfg.txt"
#define FORM_FILE_NAME		"cfg_file"

void stop_ags_server(void)
{
	FILE *script;
	char buffer[2048];

    sprintf(buffer, "/usr/local/zigor/activa/tools/suid-wrapper /usr/local/zigor/activa/tools/kill-servidor.sh");
    if((script = popen(buffer, "r")) == 0) {
		return;
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);
}

void start_ags_server(void)
{
	FILE *script;
	char buffer[2048];

    sprintf(buffer, "/usr/local/zigor/activa/tools/suid-wrapper /usr/local/zigor/activa/tools/restart-servidor.sh");
    if((script = popen(buffer, "r")) == 0) {
		return;
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);
}

int cgiMain() {
	FILE *fp;
	FILE *script;
	cgiFilePtr file;
	char buffer[1024];
	char name[1024];
	int size;
	int got;
	int da = 0;
	bool public = true;

	cgiHeaderContentType("text/html");
	fprintf(cgiOut, "<html><head>\n");
	fprintf(cgiOut, "<title>DSP Set configuration</title></head>\n");
	fprintf(cgiOut, "<body><h1>DSP Set configuration</h1>\n");

	if (cgiFormFileName(FORM_FILE_NAME, name, sizeof(name)) != cgiFormSuccess) {
		fprintf(cgiOut, "<p>No file was uploaded %d.<p>\n", cgiFormFileName(FORM_FILE_NAME, name, sizeof(name)));
		return -1;
	}
	cgiFormFileSize(FORM_FILE_NAME, &size);
	if (cgiFormFileOpen(FORM_FILE_NAME, &file) != cgiFormSuccess) {
		fprintf(cgiOut, "Could not open the file.\n");
		return -1;
	}

	fp = fopen(NEW_CFG_PATH, "w");

	while (cgiFormFileRead(file, buffer, sizeof(buffer), &got) == cgiFormSuccess)
	{
		fwrite(buffer, got, sizeof buffer[0], fp);
	}
	fclose(fp);
	cgiFormFileClose(file);

	char config[32];
	cgiFormString("config", config, sizeof config);
	fprintf(cgiOut, "<p>Config: '%s'</p>\n", config);

	public = strcmp(config, "public") == 0 ? true : false;

	cgiFormInteger("da", &da, 0);

	stop_ags_server();
    sprintf (buffer, DSP_FICH_BIN_PATH " -v -i %d -d %s -f %s -w %s %s", da, SERIAL_PORT, INI_FILE_PATH, NEW_CFG_PATH, public ? "" : "-p");
	fprintf(cgiOut, "<p>Executing %s </p>", buffer);
    if((script = popen(buffer, "r")) == 0) {
		exit(1);
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);

    sprintf (buffer, DSP_FICH_BIN_PATH " -v -i %d -d %s -f %s -r %s %s", da, SERIAL_PORT, INI_FILE_PATH, public ? PUBLIC_FILE_PATH : PRIVATE_FILE_PATH, public ? "" : "-p");
	fprintf(cgiOut, "<p>Executing %s </p>", buffer);
    if((script = popen(buffer, "r")) == 0) {
		exit(1);
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);

	fprintf(cgiOut, "</body></html>\n");
	start_ags_server();
	return 0;
}
