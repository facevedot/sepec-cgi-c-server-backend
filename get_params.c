/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ezxml.h"
#include "cgic.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define JSON_START	"{\"Parameters\": [\n"
#define JSON_END	"\t]\n}\n"
#define JSON_TEMPLATE	\
	"\t{\n\
\t\t\"name\": \"%s\",\n\
\t\t\"oid\": \"%s\",\n\
\t\t\"values\": %s,\n\
\t\t\"units\": \"%s\",\n\
\t\t\"factor\": \"%s\",\n\
\t\t\"minmax\": \"%s\",\n\
\t\t\"edit\": %s,\n\
\t\t\"type\": \"%s\",\n\
\t\t\"value\": \"%s\"\n\
\t}"

#define CMD_SNMPGET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPGET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPGET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPGET 				"snmpget -OqvU -v 2c -c zadmin localhost %s"

#define CMD_SNMPGET_VARS			CMD_SNMPGET_VAR_MIBDIR " " CMD_SNMPGET_VAR_MIBS " " CMD_SNMPGET_VAR_LD_LIB
#define CMD							CMD_SNMPGET_VARS " " CMD_SNMPGET

#define MAX_SNMPGET_RESPONSE_LEN	1024

char * get_oid_value(char const * const oid)
{
	FILE * process;
	char * response = NULL;
	char * snmpget_cmd = NULL;
	int i;

	asprintf(&snmpget_cmd, CMD, oid);
	if (snmpget_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	process = popen(snmpget_cmd, "r");
	free(snmpget_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (data[0] == '\n' && response[i-1] == '\n'){
			break;
		}
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] == '\n') {
			response[i] = data[0];
		} 
		if (data[0] != '\n') {
			response[i] = data[0];
		}
	}
	response[i] = '\0';

	pclose(process);
	return response;
}

int cgiMain() {
	cgiHeaderContentType("application/json");
	/* Top of the page */
	ezxml_t root;
	ezxml_t node;
	char filename[1024];
	char username[1024];
	char oidsToGet[4096];
	char lang[1024];
	int i = 0;
	
	cgiFormResultType result = cgiFormString("settings_xml", filename, sizeof filename);
	cgiFormString("username", username, sizeof username);
	cgiFormString("lang", lang, sizeof lang);

	if (result != cgiFormSuccess) {
		strcpy(filename, "settings_general.xml");
	}
	
	root = ezxml_parse_file(filename);
	for (node = ezxml_child(root, "node"); node; node = node->next){
		i = 0;
		ezxml_t param;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			char const * auth = ezxml_attr(param, "auth");
			if(!ezxml_attr(param, "auth")){
				strcat(oidsToGet, oid);
				strcat(oidsToGet, " ");
			}else if (strcmp(username, auth) == 0 ) {
				strcat(oidsToGet, oid);
				strcat(oidsToGet, " ");
			}
		}
	}

	char *oids = strtok (get_oid_value(oidsToGet), "\n");
	char *array[23];
	
	while (oids != NULL){
		array[i++] = oids;
		oids = strtok (NULL, "\n");
	}
	
	fprintf(cgiOut, JSON_START);
	for (node = ezxml_child(root, "node"); node; node = node->next) {
		ezxml_t param;
		i = 0;
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * auth = ezxml_attr(param, "auth");
			char *value = NULL;
			bool const is_last = param->next == NULL && node->next == NULL ? true : false;
			if(!ezxml_attr(param, "auth")){
				char const * name = ezxml_attr(param, lang);
				char const * values = ezxml_attr(param, "values");
				char const * units = ezxml_attr(param, "units");
				char const * oid = ezxml_attr(param, "oid");
				char const * factor = ezxml_attr(param, "factor");
				char const * minmax = ezxml_attr(param, "minmax");
				char const * edit = ezxml_attr(param, "edit");
				char const * type = ezxml_attr(param, "type");
				value = array[i];
				if (values[0] == '\0')
					values = "\"\"";
				if (value == NULL)
					value = "";
				if (strcmp(type, "int") == 0 && factor[0] != '\0') {
					int factor_int = atoi(factor);
					float value_float = atoi(value);
					value_float = value_float / factor_int;
					char *new_value = NULL;

					asprintf(&new_value, "%.01f", value_float);
					value = new_value;
				}
				fprintf(cgiOut, JSON_TEMPLATE "%s\n", name, oid, values, units, factor, minmax, edit, type, value, is_last ? "" : ",");
				i++;
			}else if (strcmp(username, auth) ==0 ) {
				char const * name = ezxml_attr(param, lang);
				char const * values = ezxml_attr(param, "values");
				char const * units = ezxml_attr(param, "units");
				char const * oid = ezxml_attr(param, "oid");
				char const * factor = ezxml_attr(param, "factor");
				char const * minmax = ezxml_attr(param, "minmax");
				char const * edit = ezxml_attr(param, "edit");
				char const * type = ezxml_attr(param, "type");
				value = array[i];
				if (values[0] == '\0')
					values = "\"\"";
				if (value == NULL)
					value = "";
				if (strcmp(type, "int") == 0 && factor[0] != '\0') {
					int factor_int = atoi(factor);
					float value_float = atoi(value);
					value_float = value_float / factor_int;
					char *new_value = NULL;

					asprintf(&new_value, "%.01f", value_float);
					value = new_value;
				}
				fprintf(cgiOut, JSON_TEMPLATE "%s\n", name, oid, values, units, factor, minmax, edit, type, value, is_last ? "" : ",");
				i++;
			}
		}
	}
	ezxml_free(root);
	fprintf(cgiOut, JSON_END);

	return EXIT_SUCCESS;
}
