/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ezxml.h"
#include "cgic.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define ALARM_COUNT		"zigorAlarmsPresent.0"
#define ALARM_ID		"zigorAlarmId."
#define ALARM_DESCR		"zigorAlarmDescr."
#define ALARM_TIME		"zigorAlarmTime."
#define ALARM_ELEM		"zigorAlarmElementList."
#define ALARM_COND		"zigorAlarmCondition."

#define JSON_START	"{\"Active\": [\n"
#define JSON_END	"\t]\n}\n"
#define JSON_TEMPLATE	\
	"\t{\n\
\t\t\"ID\": \"%s\",\n\
\t\t\"Descr\": \"%s\",\n\
\t\t\"Time\": \"%s\",\n\
\t\t\"Elem\": %s,\n\
\t\t\"Cond\": \"%s\"\n\
\t}"

#define CMD_SNMPGET_VAR_MIBDIR		"MIBDIRS=\"+/usr/local/zigor/activa/ags-sepec/share/mibs\""
#define CMD_SNMPGET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPGET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPGET 				"snmpget -OqvU -v 2c -c zadmin localhost %s"

#define CMD_SNMPGET_VARS			CMD_SNMPGET_VAR_MIBDIR " " CMD_SNMPGET_VAR_MIBS " " CMD_SNMPGET_VAR_LD_LIB
#define CMD							CMD_SNMPGET_VARS " " CMD_SNMPGET

#define MAX_SNMPGET_RESPONSE_LEN	1024


char * get_oid_value(char const * const oid)
{
	FILE * process;
	char * response = NULL;
	char * snmpget_cmd = NULL;
	int i;

	asprintf(&snmpget_cmd, CMD, oid);
	if (snmpget_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	process = popen(snmpget_cmd, "r");
	free(snmpget_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (data[0] == '\n' && response[i-1] == '\n'){
			break;
		}
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] == '\n') {
			response[i] = data[0];
		} 
		if (data[0] != '\n') {
			response[i] = data[0];
		}
	}
	response[i] = '\0';
	pclose(process);
	return response;
}

void translate_descr(char * description, char * ID){
    int i, j;
	ezxml_t root;
	ezxml_t node;
	ezxml_t param;
	char lang[1024];
	
	root = ezxml_parse_file("alarms_description.xml");
	cgiFormString("lang", lang, sizeof lang);
	
    for(i = 0; description[i] != '\0'; ++i)
    {
        while (!( (description[i] >= '0' && description[i] <= '9') || (description[i] == '.') || description[i] == '\0') )
        {
            for(j = i; description[j] != '\0'; ++j)
            {
                description[j] = description[j+1];
            }
            description[j] = '\0';
        }
    }
    
    for (node = ezxml_child(root, "node"); node; node = node->next) {
		for (param = ezxml_child(node, "var"); param; param = param->next) {
			char const * oid = ezxml_attr(param, "oid");
			if (strcmp(description, oid) == 0){
				strcpy(description, ezxml_attr(param, lang));
				strcpy(ID, ezxml_attr(param, "code"));
			}
		}
	}
}

int cgiMain() {
	cgiHeaderContentType("application/json");
	/* Top of the page */
	char oidsToGet[1024];
	char *array[64];
	int i = 0;
	char *n = NULL;
	
	int alarmCount = atoi(get_oid_value(ALARM_COUNT));
	
	fprintf(cgiOut, JSON_START);
	for (i = 1; i <= alarmCount; i++){
		asprintf(&n, "%d", i);
		strcat(oidsToGet, ALARM_ID);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_DESCR);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_TIME);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_ELEM);
		strcat(oidsToGet, n);
		strcat(oidsToGet, " ");
		strcat(oidsToGet, ALARM_COND);
		strcat(oidsToGet, n);
		char *oids = strtok (get_oid_value(oidsToGet), "\n");
		
		int a = 0;
		while (oids != NULL){
			array[a++] = oids;
			oids = strtok (NULL, "\n");
		}
		
		char * ELEM = array[3];
		char * time = array[2];
		char * cond = array[4];
		char * descr = array[1];
		char * id = NULL;
		id = malloc(2);
		translate_descr(descr, id);
		bool const is_last = i == alarmCount ? true : false;
		fprintf(cgiOut, JSON_TEMPLATE "%s\n", id, descr, time, ELEM, cond, is_last ? "" : ",");
		memset(oidsToGet, 0, sizeof oidsToGet);
		memset(array, 0, 64 * (sizeof array[0]) );
		free(n);
	}
	fprintf(cgiOut, JSON_END);
	return EXIT_SUCCESS;
}
