/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include "cgic.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define MAX_SNMPGET_RESPONSE_LEN	32

#define SERIAL_PORT			"/dev/ttyS0"
#define DSP_UPDATER_BIN_PATH	"/usr/local/zigor/activa/tools/dsp_updater"
#define NEW_FW_PATH			"/tmp/new_fw.S"
#define FORM_FILE_NAME		"fw_file"

void stop_ags_server(void)
{
	FILE *script;
	char buffer[2048];

    sprintf(buffer, "/usr/local/zigor/activa/tools/suid-wrapper /usr/local/zigor/activa/tools/kill-servidor.sh");
    if((script = popen(buffer, "r")) == 0) {
		return;
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);
}

void start_ags_server(void)
{
	FILE *script;
	char buffer[2048];

    sprintf(buffer, "/usr/local/zigor/activa/tools/suid-wrapper /usr/local/zigor/activa/tools/restart-servidor.sh");
    if((script = popen(buffer, "r")) == 0) {
		return;
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);
}

int cgiMain() {
	FILE *fp;
	FILE *script;
	cgiFilePtr file;
	char buffer[1024];
	char name[1024];
	int size;
	int got;
	int da = 0;

	cgiHeaderContentType("text/html");
	fprintf(cgiOut, "<html><head>\n");
	fprintf(cgiOut, "<title>DSP Firmware Upgrade</title></head>\n");
	fprintf(cgiOut, "<body><h1>DSP Firmware Upgrade</h1>\n");

	if (cgiFormFileName(FORM_FILE_NAME, name, sizeof(name)) != cgiFormSuccess) {
		fprintf(cgiOut, "<p>No file was uploaded %d.<p>\n", cgiFormFileName(FORM_FILE_NAME, name, sizeof(name)));
		return -1;
	}
	cgiFormFileSize(FORM_FILE_NAME, &size);
	if (cgiFormFileOpen(FORM_FILE_NAME, &file) != cgiFormSuccess) {
		fprintf(cgiOut, "Could not open the file.\n");
		return -1;
	}

	fp = fopen(NEW_FW_PATH, "w");

	while (cgiFormFileRead(file, buffer, sizeof(buffer), &got) == cgiFormSuccess)
	{
		fwrite(buffer, got, sizeof buffer[0], fp);
	}
	fclose(fp);
	cgiFormFileClose(file);

	cgiFormInteger("da", &da, 0);

	stop_ags_server();
    sprintf (buffer, DSP_UPDATER_BIN_PATH " -i %d -d %s -f %s", da, SERIAL_PORT, NEW_FW_PATH);
	fprintf(cgiOut, "<p>Executing %s </p>", buffer);
    if((script = popen(buffer, "r")) == 0) {
		exit(1);
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);

	fprintf(cgiOut, "</body></html>\n");
	start_ags_server();
	return 0;
}
