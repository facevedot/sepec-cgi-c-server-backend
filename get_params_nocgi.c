
#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ezxml.h"
#include "cgic.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define JSON_START	"{\"Parameters\": [\n"
#define JSON_END	"\t]\n}\n"
#define JSON_TEMPLATE	\
	"\t{\n\
\t\t\"name\": \"%s\",\n\
\t\t\"oid\": \"%s\",\n\
\t\t\"values\": %s,\n\
\t\t\"units\": \"%s\",\n\
\t\t\"factor\": \"%s\",\n\
\t\t\"minmax\": \"%s\",\n\
\t\t\"edit\": %s,\n\
\t\t\"value\": \"%s\"\n\
\t}"

#define CMD_SNMPGET_VAR_MIBDIR		"MIBDIRS=\"+/home/zigor/mibs\""
#define CMD_SNMPGET_VAR_MIBS		"MIBS=\"+ZIGOR-SMI:ZIGOR-PRODUCTS-MIB:ZIGOR-TC:ZIGOR-PARAMETER-MIB:ZIGOR-SEPEC-MIB:ZIGOR-ALARM-MIB:ZIGOR-ALARM-LOG-MIB:ZIGOR-STATUS-MIB\""
#define CMD_SNMPGET_VAR_LD_LIB	 	"LD_LIBRARY_PATH=/usr/local/lib"
#define CMD_SNMPGET 				"snmpget -OqvU -v 2c -c zadmin 192.168.33.209 %s"

#define CMD_SNMPGET_VARS			CMD_SNMPGET_VAR_MIBDIR " " CMD_SNMPGET_VAR_MIBS " " CMD_SNMPGET_VAR_LD_LIB
#define CMD							CMD_SNMPGET_VARS " " CMD_SNMPGET

#define MAX_SNMPGET_RESPONSE_LEN	1024

char * get_oid_value(char const * const oid)
{
	FILE * process;
	char * response = NULL;
	char * snmpget_cmd = NULL;
	int i;

	asprintf(&snmpget_cmd, CMD, oid);
	if (snmpget_cmd == NULL) {
		asprintf(&response, "ALLOCATION ERROR");
		return response;
	}
	process = popen(snmpget_cmd, "r");
	free(snmpget_cmd);

	if (process == NULL) {
		asprintf(&response, "READ ERROR");
		return response;
	}

	response = malloc(MAX_SNMPGET_RESPONSE_LEN);
	for (i = 0; i < MAX_SNMPGET_RESPONSE_LEN; i++) {
		char data[1];
		int error = fread(data, sizeof data[0], ARRAY_SIZE(data), process);
		if (error != sizeof data[0] * ARRAY_SIZE(data)) {
			free(response);
			asprintf(&response, "READ ERROR");
			return response;
		}
		if (data[0] != '\n') {
			response[i] = data[0];
		} else {
			break;
		}
	}
	response[i] = '\0';

	pclose(process);
	return response;
}

int main(int argc, char *argv[])
{
	/* Top of the page */
	ezxml_t f1 = ezxml_parse_file("settings.xml");
	ezxml_t node;

	fprintf(stdout, JSON_START);
	for (node = ezxml_child(f1, "node"); node; node = node->next) {
		ezxml_t param;
	    for (param = ezxml_child(node, "var"); param; param = param->next) {
	    	char const * name = ezxml_attr(param, "name");
	    	char const * oid = ezxml_attr(param, "oid");
	    	char const * values = ezxml_attr(param, "values");
	    	char const * units = ezxml_attr(param, "units");
	    	char const * factor = ezxml_attr(param, "factor");
	    	char const * minmax = ezxml_attr(param, "minmax");
	    	char const * edit = ezxml_attr(param, "edit");
	    	char *value = NULL;
	    	bool const is_last = param->next == NULL && node->next == NULL ? true : false;

	    	value = get_oid_value(oid);
	    	if (values[0] == '\0')
	    		values = "\"\"";
	    	fprintf(stdout, JSON_TEMPLATE "%s\n", name, oid, values, units, factor, minmax, edit, value, is_last ? "" : ",");
	    	free(value);
	    }
	}
	ezxml_free(f1);
	fprintf(stdout, JSON_END);

	return EXIT_SUCCESS;
}
