/* Change this if the SERVER_NAME environment variable does not report
	the true name of your web server. */
#define SERVER_NAME cgiServerName

/* You may need to change this, particularly under Windows;
	it is a reasonable guess as to an acceptable place to
	store a saved environment in order to test that feature.
	If that feature is not important to you, you needn't
	concern yourself with this. */

#define SAVED_ENVIRONMENT "/tmp/cgicsave.env"

#define _GNU_SOURCE /* For asprintf() */

#include <stdio.h>
#include "cgic.h"
#include <string.h>
#include <stdlib.h>

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof ((a)) / sizeof ((a)[0]))
#endif

#define MAX_SNMPGET_RESPONSE_LEN	32

#define INI_FILE_PATH		"/tmp/dsp.ini"
#define PUBLIC_FILE_PATH	"/tmp/publica.txt"
#define PRIVATE_FILE_PATH	"/tmp/privada.txt"
#define FORM_FILE_NAME		"ini_file"
#define SERIAL_PORT			"/dev/ttyS0"
#define DSP_FICH_BIN_PATH	"/usr/local/zigor/activa/tools/dsp_fich"

void stop_ags_server(void)
{
	FILE *script;
	char buffer[2048];

    sprintf(buffer, "/usr/local/zigor/activa/tools/suid-wrapper /usr/local/zigor/activa/tools/kill-servidor.sh");
    if((script = popen(buffer, "r")) == 0) {
		return;
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);
}

void start_ags_server(void)
{
	FILE *script;
	char buffer[2048];

    sprintf(buffer, "/usr/local/zigor/activa/tools/suid-wrapper /usr/local/zigor/activa/tools/restart-servidor.sh");
    if((script = popen(buffer, "r")) == 0) {
		return;
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);
}

int cgiMain() {
	FILE *fp;
	FILE *script;
	cgiFilePtr file;
	char name[1024];
	char buffer[4096];
	int size;
	int got;
	int da;

	cgiHeaderContentType("text/html");
	fprintf(cgiOut, "<html><head>\n");
	fprintf(cgiOut, "<title>DSP Upload INI file</title></head>\n");
	fprintf(cgiOut, "<body><h1>DSP Upload INI file</h1>\n");

	if (cgiFormFileName(FORM_FILE_NAME, name, sizeof(name)) != cgiFormSuccess) {
		fprintf(cgiOut, "<p>No file was uploaded.<p>\n");
		return -1;
	}
	cgiFormFileSize(FORM_FILE_NAME, &size);
	if (cgiFormFileOpen(FORM_FILE_NAME, &file) != cgiFormSuccess) {
		fprintf(cgiOut, "Could not open the file.\n");
		return -1;
	}

	fp = fopen(INI_FILE_PATH, "w");

	while (cgiFormFileRead(file, buffer, sizeof(buffer), &got) == cgiFormSuccess)
	{
		fwrite(buffer, got, sizeof buffer[0], fp);
	}
	fclose(fp);
	cgiFormFileClose(file);

	cgiFormInteger("da", &da, 0);

	stop_ags_server();

    sprintf (buffer, DSP_FICH_BIN_PATH " -i %d -d %s -f %s -r %s -v", da, SERIAL_PORT, INI_FILE_PATH, PUBLIC_FILE_PATH);
	fprintf(cgiOut, "<p>Executing %s </p>", buffer);
    if((script = popen(buffer, "r")) == 0) {
		exit(1);
    }
    printf("<p>");
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);

    sprintf (buffer, DSP_FICH_BIN_PATH " -i %d -d %s -p -f %s -r %s -v", da, SERIAL_PORT, INI_FILE_PATH, PRIVATE_FILE_PATH);
	fprintf(cgiOut, "<p>Executing %s </p>", buffer);
    if((script = popen(buffer, "r")) == 0) {
		exit(1);
    }
    while(!feof(script)) {
        memset(buffer, 0, sizeof buffer);
        fgets(buffer, sizeof buffer, script);
        if(buffer[0] != '\0') {
        	fprintf(cgiOut, "%s<br/>", buffer);
        }
    }
    fclose(script);

    printf("</p>");
	fprintf(cgiOut, "</body></html>\n");
	start_ags_server();
	return 0;
}
